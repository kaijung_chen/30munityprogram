﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Program that calculates area of  square, circle, rectangle and  triangle using inheritance(iShape)

public interface iShape  {
	int CaculateArea();
}
